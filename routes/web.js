const { Router } = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
// const AuthMiddleware = require('../middlewares/AuthMiddleware')

const AuthController = require('../controllers/web/AuthController')
const HomeController = require('../controllers/web/HomeController')

const web = Router()

web.use(bodyParser.json())
web.use(bodyParser.urlencoded({ extended: true }))
web.use(cookieParser())

const authController = new AuthController
const homeController = new HomeController

/// login
web.get('/login', authController.login)
web.post('/login', authController.doLogin)
web.get('/logout', authController.logout)

// web.use(AuthMiddleware())

/// add user
web.get('/add-user', homeController.add)
web.post('/save-user', homeController.saveUser)

/// update user
web.get('/edit/:id', homeController.editUser)
web.post('/save-edit/:id', homeController.updateUser)

/// delete
web.get('/delete/:id', homeController.delete)

/// user information
web.get('/user-history/:id', homeController.getHistory)
web.get('/user-biodata/:id', homeController.getBio)

web.get('/', homeController.index)


module.exports = web
